﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MusicBoard.Models;
using MusicBoard.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.ViewComponents
{
    public class RatingScaleViewComponent : ViewComponent
    {
        private MusicBoardDbContext m_db;
        public RatingScaleViewComponent(MusicBoardDbContext context)
        {
            m_db = context;
        }

        public IViewComponentResult Invoke(String nickname)
        {
            Int32[] counts = new Int32[10];

            m_db.Users.Where(u => u.Login == nickname)
                .Select(u => u.Ratings)
                .FirstOrDefault()
                .ForEach(r => counts[r.Value - 1]++);

            return View(new RatingsViewComponentModel { Ratings = counts, Nickname = nickname });
        }
    }
}
