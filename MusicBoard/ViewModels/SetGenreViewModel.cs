﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.ViewModels
{
    public class SetGenreViewModel
    {
        public Int32 AlbumId { get; set; }
        public String GenreName { get; set; }
        public AlbumAnchorViewModel AlbumAnchor { get; set; }
        public IEnumerable<GenreAnchorViewModel> AlbumGenreAnchors { get; set; }
    }
}
