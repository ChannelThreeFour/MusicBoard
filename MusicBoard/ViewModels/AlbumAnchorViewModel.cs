﻿using MusicBoard.Models;
using System;
using System.Collections.Generic;

namespace MusicBoard.ViewModels
{
    public class AlbumAnchorViewModel
    {
        public ArtistAnchorViewModel Artist { get; set; }
        public Int32 AlbumId { get; set; } 

        public String AlbumUrl { get; set; }
        public String AlbumFullName { get; set; }
        public Int32 ReleaseYear { get; set; }
        public AlbumType AlbumType { get; set; }
        public Double AverageRating { get; set; }
        public Int32 RatingsCount { get; set; }
        public Boolean HasImage { get; set; } = false;

        public GenreAnchorViewModel[] Genres { get; set; } = null;

        public AlbumAnchorViewModel(String url, String name, Int32 year, AlbumType type, Double averageRating, ArtistAnchorViewModel artist = null)
        {
            AlbumUrl = url; AlbumFullName = name;
            ReleaseYear = year; AlbumType = type;
            AverageRating = averageRating;
            Artist = artist;
        }
    }

    public class AlbumRatingViewModel : AlbumAnchorViewModel
    {
        public AlbumRatingViewModel(string url, string name, int year, AlbumType type, ArtistAnchorViewModel artist = null)
            : base(url, name, year, type, 0, artist)
        {
        }
        public Int32 Rating { get; set; }
        public DateTime RatingDate { get; set; }
    }
}