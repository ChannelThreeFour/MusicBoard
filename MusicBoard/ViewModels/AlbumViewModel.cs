﻿using MusicBoard.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.ViewModels
{
    public class AlbumViewModel
    {
        public ArtistAnchorViewModel Artist { get; set; }
        public Int32 AlbumId { get; set; }

        #region INPUT_DATA
        [Required(ErrorMessage = "Укажите id исполнителя")]
        public Int32 ArtistId { get; set; }
        
        [Required(ErrorMessage = "Укажите название альбома")]
        [MaxLength(100, ErrorMessage = "Название альбома должно быть меньше 100 символов")]
        public String AlbumFullName { get; set; }

        [Required(ErrorMessage = "Укажите url альбома")]
        [MaxLength(50, ErrorMessage = "url-адрес должен занимать меньше 50 строк")]
        [RegularExpression(@"^[a-z0-9_]+$", ErrorMessage = "url-адрес модет содержать только строчные английские символы, цифры и знак '_'")]
        public String AlbumUrlName { get; set; }

        public AlbumType AlbumType { get; set; }
        public Int32 CurrentUsersRate { get; set; }

        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }
        #endregion

        public GenreAnchorViewModel[] GenreAnchors { get; set; }
        public Int32 RatingsCount { get; set; }
        public Double AverageRating { get; set; }
        public Boolean HasImage { get; set; } = false;
    }
}
