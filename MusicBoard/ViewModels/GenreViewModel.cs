﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.ViewModels
{
    public class GenreViewModel
    {
        public Int32 GenreId { get; set; }

        public String GenreDescryption { get; set; }
        public String GenreName { get; set; }

        public List<AlbumAnchorViewModel> Albums { get; set; } = new List<AlbumAnchorViewModel>();
    }
}
