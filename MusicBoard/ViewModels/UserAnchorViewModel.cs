﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.ViewModels
{
    public class UserAnchorViewModel
    {
        public Int32 UserId { get; set; }
        public String UserName { get; set; }
        public String UserCountry { get; set; }

        public Boolean HasImage { get; set; } = false;

        public UserAnchorViewModel(String username, Int32 id, String country = null)
        {
            UserName = username; UserId = id; UserCountry = country;
        }
    }
}
