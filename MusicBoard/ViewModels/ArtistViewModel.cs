﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static MusicBoard.Controllers.ArtistController;

namespace MusicBoard.ViewModels
{
    public enum ArtistType : byte
    {
        [Display(Name = "Музыкант")]
        Person,
        [Display(Name = "Группа")]
        Band
    }
    public class ArtistViewModel
    {
        public Int32 SurrogateId { get; set; }

        [Required(ErrorMessage = "Укажите имя исполнителя в url")]
        [MaxLength(50, ErrorMessage = "url-адрес должен занимать меньше 50 строк")]
        [RegularExpression(@"^[a-z0-9_]+$", ErrorMessage = "url-адрес модет содержать только строчные английские символы, цифры и знак '_'")]
        public String UrlName { get; set; }

        [Required]
        public ArtistType Type { get; set; }

        [Required(ErrorMessage = "Укажите полное имя исолнителя")]
        [MaxLength(100, ErrorMessage = "Имя исполнителя должно быть меньше 100 символов")]
        public String FullName { get; set; }

        public Int32? YearBorn { get; set; }
        public Int32? YearDied { get; set; }

        [MaxLength(50, ErrorMessage = "Наименование страны может быть сокращено до 50 символов?")]
        public String Country { get; set; }

        [MaxLength(1000, ErrorMessage = "Длина текста не должна превышать 1000 символов")]
        [DataType(DataType.MultilineText)]
        public String AKAs { get; set; }

        [MaxLength(1000, ErrorMessage = "Длина текста не должна превышать 1000 символов")]
        [DataType(DataType.MultilineText)]
        public String RelatedBands { get; set; }

        [MaxLength(1000, ErrorMessage = "Длина текста не должна превышать 1000 символов")]
        [DataType(DataType.MultilineText)]
        public String RelatedMembers { get; set; }

        [MaxLength(10000, ErrorMessage = "Длина текста не должна превышать 10000 символов")]
        [DataType(DataType.MultilineText)]
        public String Biography { get; set; }

        public Boolean HasImage { get; set; } = false;


        public String[] AKAsLexems { get; set; }
        public String[] MembersLexems { get; set; }
        public String[] BandsLexems { get; set; }

        public ArtistAnchorDTO[] MembersAnchors { get; set; }
        public ArtistAnchorDTO[] AKAsAnchors { get; set; }
        public ArtistAnchorDTO[] BandsAnchors { get; set; }

        public AlbumAnchorViewModel[] AlbumAnchors { get; set; }
    }
}
