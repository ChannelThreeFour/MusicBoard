﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.ViewModels
{
    public enum SearchByOption
    {
        [Display(Name = "Исполнитель")]
        Artist,
        [Display(Name = "Альбом")]
        Album,
        [Display(Name = "Жанр")]
        Genre,
        [Display(Name = "Пользователь")]
        User
    }
}
