﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.ViewModels
{
    public class RatingsViewModel
    {
        public IEnumerable<AlbumRatingViewModel> Ratings { get; set; }
        public PageViewModel PageViewModel { get; set; }
        public UserAnchorViewModel UserAnchor { get; set; }
        public Int32 RatingConstraint { get; set; }
    }
}
