﻿using MusicBoard.Models;
using MusicBoard.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static MusicBoard.Controllers.ArtistController;

namespace MusicBoard.ViewModels
{
    public class UserInfoViewModel
    {
        public Int32 SurrogateId { get; set;  }

        public DateTime RegistrationDate { get; set; }

        [Required(ErrorMessage = "Укажите никнейм")]
        [MaxLength(50, ErrorMessage = "Никнейм должен занимать меньше 50 строк")]
        [RegularExpression(@"^[a-z0-9_]+$", ErrorMessage = "url-адрес модет содержать только строчные английские символы, цифры и знак '_'")]
        public String Nickname { get; set; }

        [Required(ErrorMessage = "Укажите E-mail")]
        [EmailAddress(ErrorMessage = "Некорректный Email")]
        [MaxLength(100, ErrorMessage ="Длина Email-адреса не должна превышать 100 символов")]
        public String Email { get; set; }

        [Required(ErrorMessage = "Введите пароль")]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 6, ErrorMessage ="Допустимая длина пароля: от 6 до 20 символов")]
        public String Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароль введен неверно")]
        public String ConfirmPassword { get; set; }

        [MaxLength(50, ErrorMessage ="Имя не может превышать 50 символов")]
        public String Name { get; set; }
        [MaxLength(50, ErrorMessage ="Фамилия не может превышать 50 символов")]
        public String Surname { get; set; }

        [DataType(DataType.Date)]
        public DateTime? BirthDate { get; set; }

        [MaxLength(50, ErrorMessage = "Наименование страны может быть сокращено до 50 символов?")]
        public String Country { get; set; }

        public Gender Gender { get; set; }

        [MaxLength(10000, ErrorMessage = "Длина текста не должна превышать 10000 символов")]
        public String About { get; set; }

        [MaxLength(5000, ErrorMessage = "Длина текста не должна превышать 5000 символов")]
        public String Favourites { get; set; }
        
        public Boolean HasImage { get; set; } = false;

        public List<AlbumRatingViewModel> Ratings { get; set; } = new List<AlbumRatingViewModel>();
        public Int32 RatingsCount { get; set; }

        public String[] FavouritesLexems { get; set; }
        public ArtistAnchorDTO[] FavouritesAnchors { get; set; }
    }
}
