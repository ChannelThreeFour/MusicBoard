﻿using System;

namespace MusicBoard.ViewModels
{
    public class ChartsFilterViewModel
    {
        public Int32 Year { get; set; }
        public String Genres { get; set; }
        public String Country { get; set; }
    }
}