﻿using MusicBoard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.ViewModels
{
    public class ArtistAnchorViewModel
    {
        public Int32 Id { get; set; }
        public String Url { get; set; }
        public String Name { get; set; }
        public String Country { get; set; }
        public ArtistType ArtistType { get; set; }
        public Boolean HasImage { get; set; } = false;

        public ArtistAnchorViewModel(String url, String name)
        {
            Url = url;
            Name = name;
        }
    }
}
