﻿using System;

namespace MusicBoard.ViewModels
{
    public class GenreAnchorViewModel
    {
        public Int32 GenreId { get; set; }
        public String GenreName { get; set; }
        public String GenreDescryption { get; set; }

        public GenreAnchorViewModel(Int32 id, String name)
        {
            GenreId = id; GenreName = name;
        }
    }
}