﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.ViewModels
{
    public class RatingsViewComponentModel
    {
        public Int32[] Ratings { get; set; }
        public String Nickname { get; set; }
    }
}
