﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.ViewModels
{
    public struct ArtistAnchorDTO
    {
        public String Url { get; set; }
        public String Name { get; set; }
        public ArtistAnchorDTO(String url, String name)
        {
            Url = url; Name = name;
        }
    }

}
