﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.ViewModels
{

    public class LoginViewModel
    {
        [Required(ErrorMessage = "Не указан логин")]
        public String Login { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        public String Password { get; set; }
    }

}
