﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.ViewModels
{
    public class AlbumChartsViewModel
    {
        public PageViewModel PageViewModel { get; set; }
        public IEnumerable<AlbumAnchorViewModel> AlbumAnchors { get; set; }
        public ChartsFilterViewModel ChartsFilterViewModel { get; set; }
    }
}
