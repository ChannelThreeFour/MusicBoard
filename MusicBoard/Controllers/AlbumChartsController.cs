﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MusicBoard.Models;
using MusicBoard.Services;
using MusicBoard.ViewModels;

namespace MusicBoard.Controllers
{
    public class AlbumChartsController : Controller
    {
        private MusicBoardDbContext m_db;
        public AlbumChartsController([FromServices] MusicBoardDbContext context)
        {
            m_db = context;
        }

        //[Route("charts")]
        public async Task<IActionResult> Charts([FromQuery]Int32 page = 1, ChartsFilterViewModel model = null, [FromServices] OrderbyRatingCounter counter = null)
        {
            Int32 pageSize = 40;
            Int32 pageCount = 5000;
            List<AlbumAnchorViewModel> albums = null;
            
            String[] genres = model?.Genres?.Split(',', StringSplitOptions.None).Select(s => s.Trim().ToLower()).ToArray();
            if(genres?.Any(g => String.IsNullOrWhiteSpace(g)) == true)
            {
                ModelState.AddModelError("Genres", "Неправильная расстановка запятых");
            }

            Int32[] genreIds = genres?
                .Select(genre => m_db.Genres
                    .Where(g => g.GenreName == genre)
                    .Select(g => g.GenreId)
                    .FirstOrDefault())
                .ToArray();

            StringBuilder ErrorMessage = new StringBuilder();
            for(Int32 i = 0; i < genreIds?.Length; i++)
            {
                if (genreIds[i] == default(int))
                    ErrorMessage.AppendFormat("{0}, ", genres[i]);
            }
            
            if (ErrorMessage.Length != 0)
                ModelState.AddModelError("ChartsFilterViewModel.Genres", $"Перечисленные жанры отсутствуют: {ErrorMessage.Remove(ErrorMessage.Length - 2, 2)}");

            if (ModelState.IsValid)
            {
                Func<Int32, Boolean> genreExp = a => true;
                Func<Int32, Boolean> yearExp = a => true;
                Func<String, Boolean> countryExp = a => true;

                if (genreIds != null) genreExp = g => genreIds.Any(id => id == g);
                if (model.Year != default(Int32)) yearExp = y => y == model.Year;
                if (!String.IsNullOrWhiteSpace(model.Country)) countryExp = c => c == model.Country;

                albums = await (from a in m_db.Albums.AsNoTracking()
                                where (genreIds == null || a.Genres.Any(g => genreExp(g.GenreId)))
                                    && yearExp(a.ReleaseDate.Year)
                                    && countryExp(a.Artist.ArtistCountry)
                                orderby counter.CountRate(a.AverageRating, a.RatingsCount) descending
                                select new AlbumAnchorViewModel(a.AlbumUrlName, a.AlbumFullName, a.ReleaseDate.Year, a.AlbumType, a.AverageRating,
                                            new ArtistAnchorViewModel(a.Artist.ArtistUrlName, a.Artist.ArtistFullName))
                                {
                                    RatingsCount = a.RatingsCount,
                                    AlbumId = a.Id,
                                    HasImage = a.HasImage,
                                    Genres = a.Genres.Select(gta => new GenreAnchorViewModel(gta.GenreId, gta.Genre.GenreName)).ToArray()
                                })
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync();

                if (albums.Count < pageSize) pageCount = page * pageSize;

            }
            else pageCount = 0;

            return View(new AlbumChartsViewModel {
                AlbumAnchors = albums,
                PageViewModel = new PageViewModel(pageCount, page, pageSize),
                ChartsFilterViewModel = model
            });
        }
    }


}