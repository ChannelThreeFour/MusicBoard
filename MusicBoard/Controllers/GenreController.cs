﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MusicBoard.Models;
using MusicBoard.Services;
using MusicBoard.ViewModels;

namespace MusicBoard.Controllers
{
    public class GenreController : Controller
    {
        private MusicBoardDbContext m_db;
        public GenreController(MusicBoardDbContext context)
        {
            m_db = context;
        }

        [HttpGet]
        [Authorize]
        [Route("[controller]/new")]
        public IActionResult NewGenre()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        [Route("[controller]/new")]
        public async Task<IActionResult> NewGenre(GenreViewModel model)
        {
            String genreNameIC = String.Join(' ', model.GenreName.Split(' ', StringSplitOptions.RemoveEmptyEntries)).ToLower();

            if (await m_db.Genres.AnyAsync(g => g.GenreName.Equals(genreNameIC)))
            {
                ModelState.AddModelError("GenreName", "Такой жанр уже существует");
                return View(model);
            }

            Genre genre = new Genre
            {
                GenreName = genreNameIC,
                GenreDescryption = model.GenreDescryption
            };

            await m_db.Genres.AddAsync(genre);
            await m_db.SaveChangesAsync();

            return RedirectToAction("GenreInfo", new { id = genre.GenreId });
        }

        [Route("[controller]")]
        public async Task<IActionResult> GenreInfo([FromQuery]Int32 id, [FromServices] OrderbyRatingCounter counter)
        {
            var vm = await m_db.Genres
                .Where(g => g.GenreId == id)
                .Include(g => g.AlbumsOfGenre)
                .Select(g => new GenreViewModel
                {
                    GenreId = g.GenreId,
                    GenreDescryption = g.GenreDescryption,
                    GenreName = g.GenreName,
                    Albums = g.AlbumsOfGenre
                        .OrderByDescending(a => counter.CountRate(a.Album.AverageRating, a.Album.RatingsCount))
                        .Select(a => new AlbumAnchorViewModel(
                            a.Album.AlbumUrlName,
                            a.Album.AlbumFullName,
                            a.Album.ReleaseDate.Year,
                            a.Album.AlbumType,
                            a.Album.AverageRating,
                            new ArtistAnchorViewModel(a.Album.Artist.ArtistUrlName, a.Album.Artist.ArtistFullName))
                        ).Take(50)
                        .ToList()
                }).FirstOrDefaultAsync();

            if (vm == null) return NotFound();

            return View(vm);
        }

        [Authorize]
        [Route("[controller]/set")]
        public async Task<IActionResult> SetGenre([FromQuery]Int32 albumId)
        {
            SetGenreViewModel genres = await GetSetGenreViewModelAsync(albumId, null);

            if (genres == null) return NotFound();

            return View(genres);
        }


        [HttpPost]
        [Authorize]
        [Route("[controller]/set")]
        public async Task<IActionResult> SetGenre(String genreName, Int32 albumId)
        {
            String genreNameIC = genreName.Trim().ToLower();

            Int32 genreId = await m_db.Genres
                .Where(g => g.GenreName.Equals(genreNameIC))
                .Select(g => g.GenreId)
                .FirstOrDefaultAsync();

            if (genreId == default(Int32))
            {
                ModelState.AddModelError("GenreName", "Жанр с таким названием отсутствует");
            }

            else if (await m_db.Albums.Where(a => a.Id == albumId)
                .Select(a => a.Genres.Any(g => g.GenreId == genreId))
                .FirstOrDefaultAsync())
            {
                ModelState.AddModelError("GenreName", "Этот жанр уже был добавлен в список.");
            }

            else
            {
                await m_db.AlbumOfGenreRelations.AddAsync(new GenreToAlbum { GenreId = genreId, AlbumId = albumId });
                await m_db.SaveChangesAsync();
            }

            var model = await GetSetGenreViewModelAsync(albumId, genreName);

            if (model == null) return NotFound();

            return View(model);
        }

        [Route("allgenres")]
        public async Task<IActionResult> AllGenres()
        {
            var genres = await m_db.Genres
                .Select(g => new GenreAnchorViewModel(g.GenreId, g.GenreName)
                {
                    GenreDescryption = g.GenreDescryption
                }).ToListAsync();

            return View(genres);
        }

        [NonAction]
        public async Task<SetGenreViewModel> GetSetGenreViewModelAsync(Int32 albumId, String genreName)
        {
            var genres = await m_db.Albums.Where(a => a.Id == albumId)
                    .Include(a => a.Genres)
                    .Select(a => new SetGenreViewModel
                    {
                        AlbumAnchor = new AlbumAnchorViewModel(a.AlbumUrlName,
                            a.AlbumFullName, a.ReleaseDate.Year, a.AlbumType, a.AverageRating,
                            new ArtistAnchorViewModel(a.Artist.ArtistUrlName, a.Artist.ArtistFullName)),
                        AlbumGenreAnchors = from g in a.Genres select new GenreAnchorViewModel(g.GenreId, g.Genre.GenreName),
                        AlbumId = a.Id,
                        GenreName = genreName
                    }).FirstOrDefaultAsync();

            return genres;
        }
    }
}