﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions.Internal;
using MusicBoard.Models;
using MusicBoard.Services;
using MusicBoard.ViewModels;
using Newtonsoft.Json.Linq;

namespace MusicBoard.Controllers
{
    public class ArtistController : Controller
    {
        private MusicBoardDbContext m_db;
        public ArtistController(MusicBoardDbContext context)
        {
            m_db = context;
        }

        [HttpGet]
        [Authorize]
        [Route("[controller]/new")]
        public IActionResult NewArtist()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        [Route("[controller]/new")]
        public async Task<IActionResult> NewArtist(ArtistViewModel model, [FromServices] ArtistRelationsMarkupParser parser)
        {
            if (!ModelState.IsValid) return View(model);

            if(await m_db.Artists.AnyAsync(a => a.ArtistUrlName == model.UrlName))
            {
                ModelState.AddModelError("UrlName", "Такой url существует");
            }
            if(model.YearBorn < 0 ||  model.YearBorn > DateTime.Now.Year)
            {
                ModelState.AddModelError("YearBorn", "Недопустимый год");
            }
            if(model.YearDied < 0 || model.YearDied > DateTime.Now.Year || model.YearDied < model.YearBorn)
            {
                ModelState.AddModelError("YearDied", "Недопустимый год");
            }

            String[] bands = null, members = null, akas = null;

            ArtistAnchorDTO[] bandsAnchors = null;
            ArtistAnchorDTO[] membersAnchors = null;
            ArtistAnchorDTO[] akasAnchors = null;

            try
            {
                (bands, bandsAnchors) = parser.ParseBands(model.RelatedBands);
            }
            catch(ArgumentException e)
            {
                ModelState.AddModelError("RelatedBands", e.Message);
            }

            try
            {
                (members, membersAnchors) = parser.ParseBands(model.RelatedMembers);
            }
            catch (ArgumentException e)
            {
                ModelState.AddModelError("RelatedMembers", e.Message);
            }

            try
            {
                (akas, akasAnchors) = parser.ParseBands(model.AKAs);
            }
            catch (ArgumentException e)
            {
                ModelState.AddModelError("AKAs", e.Message);
            }

            if (ModelState.IsValid)
            {
                Artist artist = new Artist
                {
                    ArtistUrlName = model.UrlName,
                    ArtistFullName = model.FullName,
                    ArtistType = model.Type,
                    ArtistYearBorn = model.YearBorn,
                    ArtistYearDied = model.YearDied,
                    ArtistCountry = model.Country,
                    ArtistBiography = model.Biography,
                    
                    ArtistRelatedBandsData = JObject.FromObject(new { lexems = bands, anchors = bandsAnchors }),
                    ArtistRelatedMembersData = JObject.FromObject(new { lexems = members, anchors = membersAnchors }),
                    ArtistAKAsData = JObject.FromObject(new { lexems = akas, anchors = akasAnchors })
                };

                await m_db.Artists.AddAsync(artist);
                await m_db.SaveChangesAsync();

                return RedirectToAction("ArtistInfo", new { url = artist.ArtistUrlName });
            }

            return View(model);
        }


        [HttpGet]
        [Route("{url}")]
        public async Task<IActionResult> ArtistInfo(String url)
        {
            var o = await m_db.Artists
                .Where(art => art.ArtistUrlName == url)
                .Select(a => new
                {
                    vm = new ArtistViewModel()
                    {
                        SurrogateId = a.ArtistId,
                        FullName = a.ArtistFullName,
                        UrlName = a.ArtistUrlName,
                        Type = a.ArtistType,
                        YearBorn = a.ArtistYearBorn,
                        YearDied = a.ArtistYearDied,
                        Country = a.ArtistCountry,
                        Biography = a.ArtistBiography,
                        HasImage = a.HasImage,
                        AlbumAnchors = (from album in a.Albums
                                        orderby album.ReleaseDate
                                        select new AlbumAnchorViewModel(
                                            album.AlbumUrlName,
                                            album.AlbumFullName,
                                            album.ReleaseDate.Year,
                                            album.AlbumType,
                                            album.AverageRating, null)
                                        {
                                            AlbumId = album.Id,
                                            HasImage = album.HasImage
                                        }).ToArray()
                    },
                    akas = a.ArtistAKAsData,
                    members = a.ArtistRelatedMembersData,
                    bands = a.ArtistRelatedBandsData,
                }).FirstOrDefaultAsync();

            if (o == null) return NotFound();

            JToken akasLexems = null, akasAnchors = null, bandsLexems = null,
                bandsAnchors = null, membersLexems = null, membersAnchors = null;

            o.akas?.TryGetValue("lexems", out akasLexems);
            o.akas?.TryGetValue("anchors", out akasAnchors);
            o.bands?.TryGetValue("lexems", out bandsLexems);
            o.bands?.TryGetValue("anchors", out bandsAnchors);
            o.members?.TryGetValue("lexems", out membersLexems);
            o.members?.TryGetValue("anchors", out membersAnchors);

            o.vm.AKAsLexems = akasLexems?.ToObject<String[]>();
            o.vm.AKAsAnchors = akasAnchors?.ToObject<ArtistAnchorDTO[]>();

            o.vm.BandsLexems = bandsLexems?.ToObject<String[]>();
            o.vm.BandsAnchors = bandsAnchors?.ToObject<ArtistAnchorDTO[]>();

            o.vm.MembersLexems = membersLexems?.ToObject<String[]>();
            o.vm.MembersAnchors = membersAnchors?.ToObject<ArtistAnchorDTO[]>();

            return View(o.vm);
        }
        
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> SetImage(Int32 artistId, IFormFile image, [FromServices] IHostingEnvironment env)
        { 
            if (image != null)
            {
                String path = env.WebRootPath + "/images/artists/" + artistId + ".jpg";
                using(var stream = new FileStream(path, FileMode.Create))
                {
                    await image.CopyToAsync(stream);
                }

                var artist = await m_db.Artists.FirstAsync(a => a.ArtistId == artistId);
                artist.HasImage = true;

                await m_db.SaveChangesAsync();

                return RedirectToAction("ArtistInfo", new { url = artist.ArtistUrlName });
            }

            return StatusCode(StatusCodes.Status204NoContent);
        }
    }
}