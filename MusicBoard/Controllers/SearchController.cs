﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MusicBoard.Models;
using MusicBoard.ViewModels;

namespace MusicBoard.Controllers
{
    public class SearchController : Controller
    {
        private MusicBoardDbContext m_db;
        public SearchController(MusicBoardDbContext context)
        {
            m_db = context;
        }

        public IActionResult SearchArtists(String context)
        {
            if (String.IsNullOrWhiteSpace(context)) return View();

            var anchors = m_db.Artists
                .Where(a => IsEquialent(context, a.ArtistFullName))
                .OrderBy(a => a.ArtistFullName.Length)
                .Take(50)
                .Select(a => new ArtistAnchorViewModel(a.ArtistUrlName, a.ArtistFullName)
                {
                    Country = a.ArtistCountry,
                    Id = a.ArtistId,
                    ArtistType = a.ArtistType
                });

            return View(anchors);
        }

        public IActionResult SearchUsers(String context)
        {
            if (String.IsNullOrWhiteSpace(context)) return View();

            var anchors = m_db.Users
                .Where(u => u.Login.StartsWith(context))
                .OrderBy(u => u.Login.Length)
                .Take(50)
                .Select(u => new UserAnchorViewModel(u.Login, u.UserId, u.Country));

            return View(anchors);
        }

        public IActionResult SearchGenres(String context)
        {
            if (String.IsNullOrWhiteSpace(context)) return View();

            var anchors = m_db.Genres
                .Where(g => IsEquialent(context, g.GenreName))
                .OrderBy(g => g.GenreName.Length)
                .Select(g => new GenreAnchorViewModel(g.GenreId, g.GenreName)
                {
                    GenreDescryption = g.GenreDescryption
                });

            return View(anchors);
        }

        public IActionResult SearchAlbums(String context)
        {
            if (String.IsNullOrWhiteSpace(context)) return View();

            var anchors = m_db.Albums
                .Where(a => IsEquialent(context, a.AlbumFullName))
                .OrderBy(a => a.AlbumFullName.Length)
                .Take(50)
                .Select(a => new AlbumAnchorViewModel(a.AlbumUrlName, a.AlbumFullName,
                                a.ReleaseDate.Year, a.AlbumType, a.AverageRating,
                                new ArtistAnchorViewModel(a.Artist.ArtistUrlName, a.Artist.ArtistFullName))
                                {
                                    AlbumId = a.Id
                                });

            return View(anchors);
        }

        public IActionResult Search(String context, SearchByOption option)
        {
            if(option == SearchByOption.Artist)
            {
                return RedirectToAction("SearchArtists", new { context });
            }

            if(option == SearchByOption.Album)
            {
                return RedirectToAction("SearchAlbums", new { context });
            }

            if (option == SearchByOption.Genre)
            {
                return RedirectToAction("SearchGenres", new { context });
            }

            if (option == SearchByOption.User)
            {
                return RedirectToAction("SearchUsers", new { context });
            }

            return NotFound();
        }

        private Boolean IsEquialent(String part, String full)
        {
            String[] partParts = part.Split(",.* &:\"\'".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            String[] fullParts = full.Split(",.* &:\"\'".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            return fullParts.Count(fullPart => partParts.Any(partPart => fullPart.StartsWith(partPart, StringComparison.InvariantCultureIgnoreCase))) >= partParts.Length;
        }
    }
}