﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MusicBoard.Models;
using MusicBoard.Services;
using MusicBoard.ViewModels;
using Newtonsoft.Json.Linq;

namespace MusicBoard.Controllers
{
    public class UserProfileController : Controller
    {
        private static Random m_rand = new Random();
        private MusicBoardDbContext m_db;

        public UserProfileController(MusicBoardDbContext context)
        {
            m_db = context;
        }

        [HttpGet]
        [Route("@{nickname}")]
        public async Task<IActionResult> UserInfo([FromRoute] String nickname)
        {
            if (!m_db.Users.Any()) return NotFound();

            var o = await m_db.Users
                .Where(u => u.Login == nickname)
                .Select(user => new
                {
                    vm = new UserInfoViewModel
                    {
                        SurrogateId = user.UserId,
                        RegistrationDate = user.RegistrationDate,
                        Nickname = user.Login,
                        Name = user.Name,
                        Surname = user.Surname,
                        Country = user.Country,
                        Gender = user.Gender,
                        About = user.About,
                        HasImage = user.HasImage,
                        BirthDate = user.BirthDate,
                        RatingsCount = user.Ratings.Count,
                        Ratings = user.Ratings
                            .OrderByDescending(r => r.Date)
                            .Take(5)
                            .Select(r => new AlbumRatingViewModel(
                                r.Album.AlbumUrlName,
                                r.Album.AlbumFullName,
                                r.Album.ReleaseDate.Year,
                                r.Album.AlbumType,
                                new ArtistAnchorViewModel(r.Album.Artist.ArtistUrlName, r.Album.Artist.ArtistFullName))
                                {
                                    AlbumId = r.AlbumId,
                                    HasImage = r.Album.HasImage,
                                    Rating = r.Value,
                                    RatingDate = r.Date,
                                    Genres = r.Album.Genres.Select(g => new GenreAnchorViewModel(g.GenreId, g.Genre.GenreName)).ToArray()
                                })
                            .ToList()
                    },
                    artistsData = user.FavouriteArtistsData
                })
                .FirstOrDefaultAsync();

            if (o == null || o.vm == null) return NotFound();

            JToken lexems = null, anchors = null;
            o.artistsData?.TryGetValue("lexems", out lexems);
            o.artistsData?.TryGetValue("anchors", out anchors);

            o.vm.FavouritesLexems = lexems?.ToObject<String[]>();
            o.vm.FavouritesAnchors = anchors?.ToObject<ArtistAnchorDTO[]>();

            return View(o.vm);
        }

        [Route("@{nickname}/[action]")]
        public async Task<IActionResult> Ratings(String nickname, [FromQuery] Int32 rating = 0, [FromQuery]Int32 page = 1)
        {
            Int32 pageSize = 50;

            var vm = await m_db.Users.Where(u => u.Login == nickname)
                .Select(u => new RatingsViewModel
                {
                    Ratings = u.Ratings
                        .Where(r => rating == 0 || r.Value == rating)
                        .OrderByDescending(r => r.Date)
                        .Skip((page - 1) * pageSize)
                        .Take(pageSize)
                        .Select(r => new AlbumRatingViewModel(
                            r.Album.AlbumUrlName,
                            r.Album.AlbumFullName,
                            r.Album.ReleaseDate.Year,
                            r.Album.AlbumType,
                            new ArtistAnchorViewModel(r.Album.Artist.ArtistUrlName, r.Album.Artist.ArtistFullName))
                        {
                            AlbumId = r.AlbumId,
                            HasImage = r.Album.HasImage,
                            Rating = r.Value,
                            RatingDate = r.Date,
                            Genres = r.Album.Genres.Select(g => new GenreAnchorViewModel(g.GenreId, g.Genre.GenreName)).ToArray()
                        }).ToList(),
                    PageViewModel = new PageViewModel(u.Ratings.Count, page, pageSize),
                    UserAnchor = new UserAnchorViewModel(u.Login, u.UserId, null),
                    RatingConstraint = rating
                }
                ).FirstOrDefaultAsync();

            if (vm == null) return NotFound();

            return View(vm);
        }


        [HttpGet]
        [Route("[action]")]
        public IActionResult Registration()
        {
            if (User.Identity.IsAuthenticated)
                return StatusCode(StatusCodes.Status403Forbidden);
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Registration([FromForm]UserInfoViewModel model, [FromServices] ArtistRelationsMarkupParser parser)
        {
            if (!ModelState.IsValid) return View(model);

            if (await m_db.Users.AnyAsync(u => u.Login == model.Nickname))
            {
                ModelState.AddModelError("Nickname", "Пользователь с таким ником уже существует");
            }

            if (model.BirthDate > DateTime.Now)
            {
                ModelState.AddModelError("BirthDate", "Указана неверная дата рождения");
            }

            String[] favourites = null;
            ArtistAnchorDTO[] favouritesAnchors = null;

            try
            {
                (favourites, favouritesAnchors) = parser.ParseBands(model.Favourites);
            }
            catch (ArgumentException e)
            {
                ModelState.AddModelError("FavouriteMusicians", e.Message);
            }

            if (ModelState.IsValid)
            {
                User user = new User
                {
                    Login = model.Nickname,
                    Password = model.Password,
                    Email = model.Email,
                    Name = model.Name,
                    Surname = model.Surname,
                    Country = model.Country,
                    Gender = model.Gender,
                    About = model.About,
                    BirthDate = model.BirthDate,
                    RegistrationDate = DateTime.Now,

                    FavouriteArtistsData = JObject.FromObject(new { lexems = favourites, anchors = favouritesAnchors })
                };

                await m_db.Users.AddAsync(user);
                await m_db.SaveChangesAsync();

                await Authenticate(user.Login);

                return RedirectToAction("UserInfo", new { nickname = user.Login });
            }
            else return View(model);
        }

        [Route("[action]")]
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
                return StatusCode(StatusCodes.Status403Forbidden);
            return View();
        }

        [HttpPost]
        [Route("[action]")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await m_db.Users.FirstOrDefaultAsync(u => u.Login == model.Login && u.Password == model.Password);
                if (user != null)
                {
                    await Authenticate(model.Login); // аутентификация

                    return RedirectToAction("UserInfo", new { nickname = model.Login });
                }
                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login");
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> SetImage(IFormFile image, [FromServices] IHostingEnvironment env)
        {
            string nickname = User?.Identity?.Name;
            if (nickname == null) return Forbid();

            if (image != null)
            {
                var user = await m_db.Users.FirstAsync(a => a.Login == nickname);

                String path = env.WebRootPath + "/images/users/" + user.UserId + ".jpg";
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await image.CopyToAsync(stream);
                }

                
                user.HasImage = true;

                await m_db.SaveChangesAsync();

                return RedirectToAction("UserInfo", new { nickname = user.Login });
            }

            return StatusCode(StatusCodes.Status204NoContent);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> SetRating(Int32 albumId, Int32 rating)
        {
            String username = User?.Identity?.Name;

            if (username != null)
            {
                var album = await m_db.Albums.FirstOrDefaultAsync(a => a.Id == albumId);
                var userId = await m_db.Users.Where(u => u.Login == username).Select(u => u.UserId).FirstOrDefaultAsync();
                var rate = await m_db.Albums.Where(a => a.Id == albumId)
                                            .Include(a => a.Ratings)
                                            .Select(a => a.Ratings.FirstOrDefault(r => r.UserId == userId))
                                            .FirstOrDefaultAsync();

                if (userId == default(Int32) || album == null) return NotFound();

                if (rate != null)
                {
                    Int32 temp = rate.Value;
                    Int32 count = album.RatingsCount;
                    m_db.Entry(rate).State = EntityState.Modified;


                    if (rating != 0)
                    {
                        rate.Value = rating;
                        rate.Date = DateTime.Now;

                        album.AverageRating = (album.AverageRating * count - temp + rating) / count;
                    }
                    else
                    {
                        m_db.Remove(rate);
                        album.AverageRating = count == 1 ? rating : (album.AverageRating * count - temp) / (count - 1);
                        album.RatingsCount--;
                    }

                    await m_db.SaveChangesAsync();
                }
                else if (rating != 0)
                {
                    album.AverageRating = (album.AverageRating * album.RatingsCount + rating) / (album.RatingsCount + 1);
                    album.RatingsCount++;

                    var link = new AlbumRating
                    {
                        AlbumId = albumId,
                        UserId = userId,
                        Date = DateTime.Now,
                        Value = rating
                    };
                    await m_db.AlbumRatings.AddAsync(link);
                    await m_db.SaveChangesAsync();
                }
                return RedirectToAction("AlbumInfo", "Album", new { url = album.AlbumUrlName });
            }

            return NotFound();
        }

        [NonAction]
        private async Task Authenticate(String login)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, login)
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
    }
}