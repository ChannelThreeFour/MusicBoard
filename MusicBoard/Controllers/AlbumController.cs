﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MusicBoard.Models;
using MusicBoard.ViewModels;

namespace MusicBoard.Controllers
{
    public class AlbumController : Controller
    {
        private MusicBoardDbContext m_db;

        public AlbumController(MusicBoardDbContext context)
        {
            m_db = context;
        }

        [HttpGet]
        [Authorize]
        [Route("[controller]/new")]
        public IActionResult NewAlbum()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        [Route("[controller]/new")]
        public async Task<IActionResult> NewAlbum(AlbumViewModel model)
        {
            if(await m_db.Albums.AnyAsync(a => a.AlbumUrlName == model.AlbumUrlName))
            {
                ModelState.AddModelError("AlbumUrlName", "Альбом с указанным url-адресом уже существует");
            }

            var artist_vm = await m_db.Artists.Where(a => a.ArtistId == model.ArtistId)
                .Select(a => new ArtistAnchorViewModel(a.ArtistUrlName, a.ArtistFullName))
                .FirstOrDefaultAsync();

            if(artist_vm == null)
            {
                ModelState.AddModelError("ArtistId", "Артиста с таким id не существует");
            }

            if (ModelState.IsValid)
            {
                Album album = new Album
                {
                    ArtistId = model.ArtistId,
                    AlbumFullName = model.AlbumFullName,
                    AlbumUrlName = model.AlbumUrlName,
                    AlbumType = model.AlbumType,
                    ReleaseDate = model.ReleaseDate
                };

                await m_db.Albums.AddAsync(album);
                await m_db.SaveChangesAsync();

                return RedirectToAction("AlbumInfo", new { url = album.AlbumUrlName });
            }
            return View(model);
        }

        [HttpGet]
        [Route("[controller]/{url}")]
        public async Task<IActionResult> AlbumInfo(String url)
        {
            var vm = await m_db.Albums
                .Where(a => a.AlbumUrlName == url)
                .Include(a => a.Ratings)
                .Include(a => a.Genres)
                .Select(a => new AlbumViewModel
                {
                    Artist = new ArtistAnchorViewModel(a.Artist.ArtistUrlName, a.Artist.ArtistFullName),
                    AlbumId = a.Id,
                    AlbumFullName = a.AlbumFullName,
                    AlbumType = a.AlbumType,
                    ReleaseDate = a.ReleaseDate,
                    RatingsCount = a.RatingsCount,
                    AverageRating = a.AverageRating,
                    HasImage = a.HasImage,
                    GenreAnchors = (from g in a.Genres select 
                                   new GenreAnchorViewModel(g.Genre.GenreId, g.Genre.GenreName)).ToArray()
                })
                .FirstOrDefaultAsync();

            if (vm == null)
            {
                return NotFound();
            }

            String username = User?.Identity?.Name;

            if (username != null)
            {
                var rating = await m_db.Users.Where(u => u.Login == username)
                        .Include(u => u.Ratings)
                        .Select(u => u.Ratings.FirstOrDefault(r => r.AlbumId == vm.AlbumId))
                        .FirstOrDefaultAsync();

                if(rating != null) vm.CurrentUsersRate = rating.Value;
            }

            return View(vm);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> SetImage(Int32 albumId, IFormFile image, [FromServices] IHostingEnvironment env)
        {
            if (image != null)
            {
                String path = env.WebRootPath + "/images/albums/" + albumId + ".jpg";
                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await image.CopyToAsync(stream);
                }

                var album = await m_db.Albums.FirstAsync(a => a.Id == albumId);
                album.HasImage = true;

                await m_db.SaveChangesAsync();

                return RedirectToAction("AlbumInfo", new { url = album.AlbumUrlName });
            }

            return StatusCode(StatusCodes.Status204NoContent);
        }
    }
}