﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.Services
{
    public static class OrderbyRatingCounterExtensions
    {
        public static void AddOrderbyRatingCounter(this IServiceCollection services)
        {
            services.AddTransient<OrderbyRatingCounter>();
        }
    }
    public class OrderbyRatingCounter
    {
        public Double CountRate(Double avg, Int32 count)
        {
            Int32 k;

            if (count < 8) k = 10;
            else if (count < 16) k = avg < 3 ? 9 : 11;
            else if (count < 64) k = avg < 3 ? 7 : 13;
            else if (count < 256) k = avg < 3 ? 5 : 15;
            else if (count < 512) k = avg < 3 ? 4 : 16;
            else if (count < 1024) k = avg < 3 ? 3 : 17;
            else if (count < 2048) k = avg < 3 ? 2 : 18;
            else k = avg < 3 ? 1 : 19;

            return k * avg;
        }
    }
}
