﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using MusicBoard.Models;
using MusicBoard.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicBoard.Services
{
    public static class MarkupParserExtensions
    {
        public static void AddArtistRelationsMarkupParser(this IServiceCollection services)
        {
            services.AddTransient<ArtistRelationsMarkupParser>();
        }
    }
    public class ArtistRelationsMarkupParser
    {
        private MusicBoardDbContext m_db;
        private const String LEFT_BRACKET_MSG  = "Отсутствует открывающая скобка '['";
        private const String RIGHT_BRACKET_MSG = "Отсутствует закрывающая скобка ']'";
        private const String ANCHOR_FORMAT_MSG = "Нарушен формат id. Содержимое скобок не может быть пусто";
        private const String ID_DESCRYPTOR_MSG = "Нарушен формат id. Отсутствует знак '@'";
        private const String NOTANUMBER_MSG = "id должен быть выражен числом";
        private const String NOTFOUND_MSG = "Исполнитель с таким id не найден";

        public ArtistRelationsMarkupParser(MusicBoardDbContext context)
        {
            m_db = context;
        }

        public (String[], ArtistAnchorDTO[]) ParseBands(String text)
        {
            if (text == null) return (null, null);

            Int32 brackets_cntr = 0;
            for(Int32 i=0; i<text.Length; i++)
            {
                if (text[i] == '[') brackets_cntr++;
                else if (text[i] == ']') brackets_cntr--;

                if (brackets_cntr > 1) throw new ArgumentException(RIGHT_BRACKET_MSG);
                if (brackets_cntr < 0) throw new ArgumentException(LEFT_BRACKET_MSG);
            }

            if (brackets_cntr != 0) throw new ArgumentException(RIGHT_BRACKET_MSG);

            String[] lexems = text.Split("[]".ToCharArray(), StringSplitOptions.None);

            IList<ArtistAnchorDTO> anchors = new List<ArtistAnchorDTO>();
            IList<String> strs = new List<String>();

            for(Int32 i = 0; i < lexems.Length; i++)
            {
                String substr = lexems[i];
                if (i % 2 == 0)
                {
                    strs.Add(substr);
                }
                else
                {
                    if (String.IsNullOrWhiteSpace(substr))
                    {
                        throw new ArgumentException(ANCHOR_FORMAT_MSG);
                    }
                    if (substr[0] != '@')
                    {
                        throw new ArgumentException(ID_DESCRYPTOR_MSG);
                    }
                    if (!Int32.TryParse(substr.Substring(1), out Int32 id))
                    {
                        throw new ArgumentException(NOTANUMBER_MSG);
                    }
                    else
                    {
                        var anchor_info = m_db.Artists
                            .Where(a => a.ArtistId == id)
                            .Select(a => new ArtistAnchorDTO(a.ArtistUrlName, a.ArtistFullName))
                            .FirstOrDefault();

                        if(anchor_info.Equals(default(ArtistAnchorDTO)))
                        {
                            throw new ArgumentException(NOTFOUND_MSG);
                        }
                        else
                        {
                            anchors.Add(anchor_info);
                        }
                    }
                }
            }

            return (strs.ToArray(), anchors.ToArray());
        }
    }
}
