﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MusicBoard.Models
{
    public class Genre
    {
        public Int32 GenreId { get; set; }

        public String GenreName { get; set; }
        public String GenreDescryption { get; set; }

        [InverseProperty("Genre")]
        public List<GenreToAlbum> AlbumsOfGenre { get; set; } = new List<GenreToAlbum>();
    }
}