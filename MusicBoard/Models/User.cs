﻿using MusicBoard.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MusicBoard.Models
{
    public enum Gender : byte
    {
        [Display(Name = "жен.")]
        Female = 1,
        [Display(Name = "муж.")]
        Male = 2
    }

    public class User
    {
        public Int32 UserId { get; set; }

        [Required]
        public String Login { get; set; }
        [Required]
        public String Password { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime RegistrationDate { get; set; }
        [Required]
        public String Email { get; set; }

        public String Name { get; set; }
        public String Surname { get; set; }

        public Boolean HasImage { get; set; } = false;

        [DataType(DataType.DateTime)]
        public DateTime? BirthDate { get; set; }

        public String Country { get; set; }
        public Gender Gender { get; set; }
        public String About { get; set; }

        [InverseProperty("User")]
        public List<AlbumRating> Ratings { get; set; } = new List<AlbumRating>();

        [NotMapped]
        public JObject FavouriteArtistsData { get; set; }

        public String FavouriteArtists
        {
            get { return FavouriteArtistsData?.ToString(); }
            set { FavouriteArtistsData = JsonConvert.DeserializeObject<JObject>(value ?? ""); }
        }

       // public List<UsersFavouriteArtist> FavouriteArtists { get; set; } = new List<UsersFavouriteArtist>();
    }
}