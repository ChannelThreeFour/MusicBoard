﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.Models
{
    public class AlbumRating
    {
        public Int32 Id { get; set; }

        public Int32 AlbumId { get; set; }
        public Album Album { get; set; }

        public Int32 UserId { get; set; }
        public User User { get; set; }

        public Int32 Value { get; set; }
        public DateTime Date { get; set; }
    }
}
