﻿using MusicBoard.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.Models
{
    public class Artist
    {
        [Required]
        public Int32 ArtistId { get; set; }

        [Required]
        public String ArtistUrlName { get; set; }
        [Required]
        public String ArtistFullName { get; set; }

        public ArtistType ArtistType { get; set; }
        public Int32? ArtistYearBorn { get; set; }
        public Int32? ArtistYearDied { get; set; }

        public String ArtistCountry { get; set; }
        public String ArtistBiography { get; set; }
        public Boolean HasImage { get; set; } = false;

        [InverseProperty("Artist")]
        public List<Album> Albums { get; set; } = new List<Album>();

        [NotMapped]
        public JObject ArtistRelatedBandsData { get; set; }

        [NotMapped]
        public JObject ArtistRelatedMembersData { get; set; }

        [NotMapped]
        public JObject ArtistAKAsData { get; set; }

        public String RelatedBands
        {
            get { return ArtistRelatedBandsData?.ToString(); }
            set { ArtistRelatedBandsData = JsonConvert.DeserializeObject<JObject>(value ?? ""); }
        }

        public String RelatedMembers
        {
            get { return ArtistRelatedMembersData?.ToString(); }
            set { ArtistRelatedMembersData = JsonConvert.DeserializeObject<JObject>(value ?? ""); }
        }

        public String ArtistAKAs
        {
            get { return ArtistAKAsData?.ToString(); }
            set { ArtistAKAsData = JsonConvert.DeserializeObject<JObject>(value ?? ""); }
        }

        //[InverseProperty("Member")]
        //public List<ArtistMembership> ArtistRelatedBands { get; set; } = new List<ArtistMembership>();

        //[InverseProperty("Band")]
        //public List<ArtistMembership> ArtistRelatedMembers { get; set; } = new List<ArtistMembership>();
    }
}
