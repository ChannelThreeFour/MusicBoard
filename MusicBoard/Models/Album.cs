﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.Models
{
    public enum AlbumType : byte
    {
        [Display(Name = "Альбом")]
        Album,
        [Display(Name = "Сингл")]
        Single,
        [Display(Name = "Концертный альбом")]
        LiveAlbum,
        [Display(Name = "Сборник")]
        Compilation,
        [Display(Name = "EP")]
        EP,
        [Display(Name = "DJ Set")]
        DJSet,
        [Display(Name = "Саундтрек к фильму")]
        MovieSoundtrack,
        [Display(Name = "Саундтрек к видеоигре")]
        VideoGameSoundtrack
    }

    public class Album
    {
        public Artist Artist { get; set; }
        [Required]
        public Int32 ArtistId { get; set; }

        public Int32 Id { get; set; }

        [Required]
        public String AlbumFullName { get; set; }
        [Required]
        public String AlbumUrlName { get; set; }

        [Required]
        public AlbumType AlbumType { get; set; }

        public DateTime ReleaseDate { get; set; }

        public Int32 RatingsCount { get; set; }
        public Double AverageRating { get; set; }

        public Boolean HasImage { get; set; } = false;

        [InverseProperty("Album")]
        public List<GenreToAlbum> Genres { get; set; } = new List<GenreToAlbum>();

        [InverseProperty("Album")]
        public List<AlbumRating> Ratings { get; set; } = new List<AlbumRating>();

        [NotMapped]
        public JObject TracklistData { get; set; }

        public String Tracklist
        {
            get { return TracklistData?.ToString(); }
            set { TracklistData = JsonConvert.DeserializeObject<JObject>(value ?? ""); }
        }
    }
}
