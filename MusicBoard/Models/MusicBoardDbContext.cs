﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.Models
{
    public class MusicBoardDbContext : DbContext
    {
        public MusicBoardDbContext(DbContextOptions<MusicBoardDbContext> options) 
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<GenreToAlbum> AlbumOfGenreRelations { get; set; }
        public DbSet<AlbumRating> AlbumRatings { get; set; }
        //public DbSet<ArtistMembership> MembershipRelations { get; set; }
        //public DbSet<UsersFavouriteArtist> UsersFavouriteArtists { get; set; }
    }
}
