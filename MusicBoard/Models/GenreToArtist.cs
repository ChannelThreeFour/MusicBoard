﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.Models
{
    public class GenreToAlbum
    {
        public Int32 Id { get; set; }

        public Genre Genre { get; set; }
        public Int32 GenreId { get; set; }

        public Album Album { get; set; }
        public Int32 AlbumId { get; set; }

        //priority
    }
}
