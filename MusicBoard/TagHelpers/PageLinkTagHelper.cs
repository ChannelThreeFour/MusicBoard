﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.AspNetCore.Routing;
using MusicBoard.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicBoard.TagHelpers
{
    public class PageLinkTagHelper : TagHelper
    {
        private IUrlHelperFactory m_urlHelperFactory;
        public PageLinkTagHelper(IUrlHelperFactory helperFactory)
        {
            m_urlHelperFactory = helperFactory;
        }

        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }

        [HtmlAttributeName("page-model")]
        public PageViewModel PageModel { get; set; }

        [HtmlAttributeName("page-action")]
        public string PageAction { get; set; }

        [HtmlAttributeName("asp-all-route-data", DictionaryAttributePrefix = "asp-route-")]
        public IDictionary<string, string> RouteValues
        {
            get
            {
                if (m_routeValues == null)
                    m_routeValues = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                return m_routeValues;
            }
            set { m_routeValues = value; }
        }

        private IDictionary<string, string> m_routeValues;

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            IUrlHelper urlHelper = m_urlHelperFactory.GetUrlHelper(ViewContext);
            output.TagName = "div";

            TagBuilder tag = new TagBuilder("ul");
            tag.AddCssClass("pagination");

            TagBuilder currentItem = CreateTag(PageModel.PageNumber, urlHelper);
            if (PageModel.HasPreviousPage)
            {
                TagBuilder prevItem = CreateTag(PageModel.PageNumber - 1, urlHelper);
                tag.InnerHtml.AppendHtml(prevItem);
            }
            tag.InnerHtml.AppendHtml(currentItem);
            if (PageModel.HasNextPage)
            {
                TagBuilder nextItem = CreateTag(PageModel.PageNumber + 1, urlHelper);
                tag.InnerHtml.AppendHtml(nextItem);
            }
            output.Content.AppendHtml(tag);
        }

        TagBuilder CreateTag(int pageNumber, IUrlHelper urlHelper)
        {
            TagBuilder a = new TagBuilder("a");
            TagBuilder li = new TagBuilder("li");

            li.AddCssClass("page-item");
            a.AddCssClass("page-link");

            if (pageNumber == PageModel.PageNumber)
            {
                li.AddCssClass("active");
            }
            else
            {
                var routeValues = new RouteValueDictionary(m_routeValues);
                routeValues.Add("page", pageNumber);
                a.Attributes["href"] = urlHelper.Action(PageAction, routeValues);
            }
            a.InnerHtml.Append(pageNumber.ToString());
            li.InnerHtml.AppendHtml(a);

            return li;
        }
    }
}
