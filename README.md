﻿## Прочтите это перед установкой!!!

## Порядок запуска сервера
1. установите .NET core SDK под Windows с сайта [Microsoft](https://www.microsoft.com/net/download/windows/build)
2. откройте в консоли директорию содержащую файл MusicBoard.csproj
3. пропишите в консоли команды в следующем порядке
>`dotnet ef database update`
>`dotnet run`
4. выполнение команд инициализирует запуск приложения и может занять некоторое время
5. по окончании выполнения последней команды в консоль будет выведен адрес http-запроса, скопируйте его и вставьте в адресную строку браузера

На данный момент готовы финальные версии:

* формы регистрации пользователя `/Registration`
* страницы профиля пользователя `/@__url__`
* формы авторизации `/login`
* формы добавления страницы исполнителя/группы `/artist/new`
* страницы профиля исполнителя `/__url__`
* страницы добавления альбома `/album/new`
* страницы добавления жанра `/genre/new`
* страницы профиля жанра
* страниц поиска сущностей по наименованию
* страницы чартов с фильтрами поиска
* добавления оценок к альбомам


## Добавление ссылок на другие страницы

При регистрации нового исполнителя, можно указать связанных с ним исполнителей 
в графе "Участники", "Группы", "Известен(ы) также как". Для этого вместо имени нужно указать их id со знаком '@' в квадратных '[' скобках ']'

Например,

>`[@203] (1967-1983), [@234] (1968-1994, 2014)`

превратится в

>[Roger Waters](#) (1967-1983), [David Gilmour](#) (1968-1994, 2014)

при условии, что оба исполнителя были зарегистрирваны ранее.


Аналогично можно заполнить поле "Любимые исполнители" при регистрации пользователя.

## Об архитектуре

В папке [MusicBoard/Controllers](MusicBoard/Controllers) расположены классы контроллеров. 
Большинство из них конструируют объекты моделей-представлений, расположенных в папке
[MusicBoard/ViewModels](MusicBoard/ViewModels), заполняя их данными из моделей 
[MusicBoard/Models](MusicBoard/Models), и передают в представления [MusicBoard/Views](MusicBoard/Views).

Представления, возвращаемые из методов контроллера имеют имя метода и расположены в папке по имени
контроллера.


Представления, если не указано иное, загружаются в тело "главного" представления 
[_Layout.cshtml](MusicBoard/Views/Shared/_Layout.cshtml), расположенного в папке
[MusicBoard/Views/Shared](MusicBoard/Views/Shared), где находятся и прочие "общие"
компоненты представлений.

## О статических файлах

В папке [MusicBoard/wwwroot](MusicBoard/wwwroot) расположены статические css, js-файлы
и изображения.